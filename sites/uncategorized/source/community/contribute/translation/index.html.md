---
layout: markdown_page
title: "Contributing to Translation"
description: "Not all GitLab pages have been updated to be translatable, all languages other than English are work-in-progress as more strings are being externalized."
canonical_path: "/community/contribute/translation/"
---

## Translation

Please note that not all GitLab pages have been updated to be translatable, and all languages other than English are `work-in-progress` as more strings are being externalized. For more information visit [the documentation](https://docs.gitlab.com/ee/development/i18n/translation.html).

1. Visit our [Crowdin project](https://translate.gitlab.com/) and sign up.
1. Find a language you’d like to contribute to.
1. Improve existing translations, vote on new translations, and/or contribute new translations to your given language.
1. Once approved, your changes will automatically be included in the next version of GitLab!